use criterion::{black_box, criterion_group, criterion_main, Criterion};
use tasm::indicators::atr::ATR;
use tasm::indicators::bb::BB;
use tasm::indicators::bbp::BBP;
use tasm::indicators::bbw::BBW;
use tasm::indicators::change::Change;
use tasm::indicators::dev::Dev;
use tasm::indicators::ema::EMA;
use tasm::indicators::ewma::EWMA;
use tasm::indicators::highest::Highest;
use tasm::indicators::hma::HMA;
use tasm::indicators::lowest::Lowest;
use tasm::indicators::price_volume::PriceVolume;
use tasm::indicators::rma::RMA;
use tasm::indicators::roc::ROC;
use tasm::indicators::rsi::RSI;
use tasm::indicators::sma::SMA;
use tasm::indicators::stdev::StDev;
use tasm::indicators::tr::TR;
use tasm::indicators::traits::IndicatorTraits;
use tasm::indicators::wma::WMA;
use tasm::tools::bench::OHLCV;

use tasm::indicators::pai::PAI;
use tasm::indicators::percent_rank::PercentRank;
use tasm::indicators::psar::PSAR;
use tasm::indicators::stoch::Stoch;
use tasm::indicators::svo::SVO;
use tasm::indicators::vwma::VWMA;

fn bench_atr(data: &Vec<(f64, f64, f64)>) {
    let mut atr = ATR::new(9);
    for value in data.iter() {
        atr.next(*value);
    }
}

fn bench_bb(data: &Vec<f64>) {
    let mut bb = BB::<f64>::new(9, 2.0);
    for value in data.iter() {
        bb.next(*value);
    }
}

fn bench_bbp(data: &Vec<f64>) {
    let mut bbp = BBP::<f64>::new(9, 2.0);
    for value in data.iter() {
        bbp.next(*value);
    }
}

fn bench_bbw(data: &Vec<f64>) {
    let mut bbw = BBW::<f64>::new(9, 2.0);
    for value in data.iter() {
        bbw.next(*value);
    }
}

fn bench_change(data: &Vec<f64>) {
    let mut change = Change::<f64>::new(9);
    for value in data.iter() {
        change.next(*value);
    }
}

fn bench_dev(data: &Vec<f64>) {
    let mut dev = Dev::<f64>::new(9);
    for value in data.iter() {
        dev.next(*value);
    }
}

fn bench_ema(data: &Vec<f64>) {
    let mut ema = EMA::<f64>::new(9);
    for value in data.iter() {
        ema.next(*value);
    }
}

fn bench_ewma(data: &Vec<f64>) {
    let mut ewma = EWMA::<f64>::new(9);
    for value in data.iter() {
        ewma.next(*value);
    }
}

fn bench_highest(data: &Vec<f64>) {
    let mut highest = Highest::<f64>::new(9);
    for value in data.iter() {
        highest.next(*value);
    }
}

fn bench_hma(data: &Vec<f64>) {
    let mut hma = HMA::<f64>::new(9);
    for value in data.iter() {
        hma.next(*value);
    }
}

fn bench_lowest(data: &Vec<f64>) {
    let mut lowest = Lowest::<f64>::new(9);
    for value in data.iter() {
        lowest.next(*value);
    }
}

fn bench_pai(data: &Vec<(f64, f64, f64)>) {
    let mut pai = PAI::<f64>::new(9, 3, 20);
    for value in data.iter() {
        pai.next(*value);
    }
}

fn bench_percent_rank(data: &Vec<f64>) {
    let mut percent_rank = PercentRank::<f64>::new(9);
    for value in data.iter() {
        percent_rank.next(*value);
    }
}

fn bench_price_volume(data: &Vec<(f64, f64)>) {
    let mut price_volume = PriceVolume::<f64>::new(9, 9, 1.0);
    for value in data.iter() {
        price_volume.next(*value);
    }
}

fn bench_psar(data: &Vec<(f64, f64)>) {
    let mut psar = PSAR::<f64>::new(0.02, 0.02, 0.2);
    for value in data.iter() {
        psar.next(*value);
    }
}

fn bench_rma(data: &Vec<f64>) {
    let mut rma = RMA::<f64>::new(9);
    for value in data.iter() {
        rma.next(*value);
    }
}

fn bench_roc(data: &Vec<f64>) {
    let mut roc = ROC::<f64>::new(9);
    for value in data.iter() {
        roc.next(*value);
    }
}

fn bench_rsi(data: &Vec<f64>) {
    let mut rsi = RSI::<f64>::new(9);
    for value in data.iter() {
        rsi.next(*value);
    }
}

fn bench_sma(data: &Vec<f64>) {
    let mut sma = SMA::<f64>::new(9);
    for value in data.iter() {
        sma.next(*value);
    }
}

fn bench_stdev(data: &Vec<f64>) {
    let mut stdev = StDev::<f64>::new(9);
    for value in data.iter() {
        stdev.next(*value);
    }
}

fn bench_stoch(data: &Vec<(f64, f64, f64)>) {
    let mut stoch = Stoch::<f64>::new(9, 3, 3);
    for value in data.iter() {
        stoch.next(*value);
    }
}

fn bench_svo(data: &Vec<f64>) {
    let mut stoch = SVO::<f64>::new(9, 18);
    for value in data.iter() {
        stoch.next(*value);
    }
}

fn bench_tr(data: &Vec<(f64, f64, f64)>) {
    let mut tr = TR::<f64>::new();
    for value in data.iter() {
        tr.next(*value);
    }
}

fn bench_wma(data: &Vec<f64>) {
    let mut wma = WMA::<f64>::new(9);
    for value in data.iter() {
        wma.next(*value);
    }
}

fn bench_vwma(data: &Vec<(f64, f64)>) {
    let mut vwma = VWMA::<f64>::new(9);
    for value in data.iter() {
        vwma.next(*value);
    }
}

fn criterion_benchmark(c: &mut Criterion) {
    // Prepare inputs
    let bench_size = 100000;
    let ohlcv = OHLCV::generate_random_ohlcv_series(bench_size);

    let mut close: Vec<f64> = Vec::with_capacity(bench_size);
    let mut volume: Vec<f64> = Vec::with_capacity(bench_size);
    let mut close_volume: Vec<(f64, f64)> = Vec::with_capacity(bench_size);
    let mut high_low: Vec<(f64, f64)> = Vec::with_capacity(bench_size);
    let mut high_low_close: Vec<(f64, f64, f64)> = Vec::with_capacity(bench_size);
    for i in 0..bench_size {
        close.push(ohlcv[i].close);
        volume.push(ohlcv[i].volume);
        close_volume.push((ohlcv[i].close, ohlcv[i].volume));
        high_low.push((ohlcv[i].high, ohlcv[i].low));
        high_low_close.push((ohlcv[i].high, ohlcv[i].low, ohlcv[i].close));
    }

    // Bench
    c.bench_function("ATR 100K", |b| {
        b.iter(|| bench_atr(black_box(&high_low_close)))
    });
    c.bench_function("BB 100K", |b| b.iter(|| bench_bb(black_box(&close))));
    c.bench_function("BBP 100K", |b| b.iter(|| bench_bbp(black_box(&close))));
    c.bench_function("BBW 100K", |b| b.iter(|| bench_bbw(black_box(&close))));
    c.bench_function("Change 100K", |b| {
        b.iter(|| bench_change(black_box(&close)))
    });
    c.bench_function("Dev 100K", |b| b.iter(|| bench_dev(black_box(&close))));
    c.bench_function("EMA 100K", |b| b.iter(|| bench_ema(black_box(&close))));
    c.bench_function("EWMA 100K", |b| b.iter(|| bench_ewma(black_box(&close))));
    c.bench_function("Highest 100K", |b| {
        b.iter(|| bench_highest(black_box(&close)))
    });
    c.bench_function("HMA 100K", |b| b.iter(|| bench_hma(black_box(&close))));
    c.bench_function("Lowest 100K", |b| {
        b.iter(|| bench_lowest(black_box(&close)))
    });
    c.bench_function("PAI 100K", |b| {
        b.iter(|| bench_pai(black_box(&high_low_close)))
    });
    c.bench_function("Percent Rank 100K", |b| {
        b.iter(|| bench_percent_rank(black_box(&close)))
    });
    c.bench_function("Price Volume 100K", |b| {
        b.iter(|| bench_price_volume(black_box(&close_volume)))
    });
    c.bench_function("PSAR 100K", |b| b.iter(|| bench_psar(black_box(&high_low))));
    c.bench_function("RMA 100K", |b| b.iter(|| bench_rma(black_box(&close))));
    c.bench_function("ROC 100K", |b| b.iter(|| bench_roc(black_box(&close))));
    c.bench_function("RSI 100K", |b| b.iter(|| bench_rsi(black_box(&close))));
    c.bench_function("SMA 100K", |b| b.iter(|| bench_sma(black_box(&close))));
    c.bench_function("StDev 100K", |b| b.iter(|| bench_stdev(black_box(&close))));
    c.bench_function("Stoch 100K", |b| {
        b.iter(|| bench_stoch(black_box(&high_low_close)))
    });
    c.bench_function("SVO 100K", |b| b.iter(|| bench_svo(black_box(&volume))));
    c.bench_function("TR 100K", |b| {
        b.iter(|| bench_tr(black_box(&high_low_close)))
    });
    c.bench_function("WMA 100K", |b| b.iter(|| bench_wma(black_box(&close))));
    c.bench_function("VWMA 100K", |b| {
        b.iter(|| bench_vwma(black_box(&close_volume)))
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
