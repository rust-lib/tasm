use std::sync::Arc;

#[derive(Debug, PartialEq, Clone)]
pub struct DataPoint<T: Clone> {
    data: Vec<T>,
}

impl<T: Clone> DataPoint<T> {
    pub fn new(vec: Vec<T>) -> Self {
        DataPoint { data: vec }
    }

    pub fn get_arc(&self) -> Arc<DataPoint<T>> {
        Arc::new(self.clone())
    }

    pub fn iter(&self) -> impl Iterator<Item = &T> {
        self.data.iter()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::sync::Arc;
    use std::thread;

    #[test]
    fn load_arc_data_point() {
        let vec: Vec<f64> = vec![1.0, 2.0, 3.0, 4.0, 5.0];
        let data_point = DataPoint::new(vec.clone());
        let arc_data_point = data_point.get_arc();

        let thread_data_point = Arc::clone(&arc_data_point);
        let expected_data_point = DataPoint::new(vec);

        // Spawn thread
        let response_data_point = thread::spawn(move || (*thread_data_point).clone())
            .join()
            .unwrap();

        assert_eq!(expected_data_point, response_data_point);
    }
}
