// TODO: Improve perf
use super::traits::IndicatorTraits;
use crate::tools::cache::Cache;
use num_traits::{Float, NumAssign};
use std::fmt::Debug;

#[derive(Debug, Clone)]
pub struct PercentRank<T: Float + NumAssign + Debug> {
    period: usize,
    cache: Cache<T>,
    t_period: T,
    t_100: T,
    t_1: T,
}

impl<T: Float + NumAssign + Debug> PercentRank<T> {
    pub fn new(period: usize) -> Self {
        PercentRank {
            period,
            cache: Cache::new(period),
            t_period: T::from(period).unwrap(),
            t_100: T::from(100).unwrap(),
            t_1: T::one(),
        }
    }

    fn calc(&self, value: T) -> T {
        let mut count = T::zero();

        for cache_value in self.cache.iter() {
            if cache_value <= &value {
                count += self.t_1;
            }
        }

        count / self.t_period * self.t_100
    }
}

impl<T: Float + NumAssign + Debug> IndicatorTraits<T, T> for PercentRank<T> {
    fn get_init_len(&self) -> usize {
        self.period
    }

    #[inline]
    fn next(&mut self, value: T) -> T {
        let percent_rank = self.calc(value);
        self.cache.push(value);
        percent_rank
    }

    fn live(&self, value: T) -> T {
        let percent_rank = self.calc(value);
        percent_rank
    }
}
#[cfg(test)]
mod tests {
    use super::*;
    use crate::indicators::test_data::get_test_data;
    // use crate::tools::test::assert_vec_nearly_equal;
    use serde::{Deserialize, Serialize};
    use std::io::Error;

    #[derive(Serialize, Deserialize, Debug)]
    struct TestData {
        period: usize,
        init_len: usize,
        input: Vec<f64>,
        expected: Vec<f64>,
    }

    #[test]
    fn test_percent_rank() -> Result<(), Error> {
        let file_path = "src/indicators/test_data/indicators/percentRank.json";

        match get_test_data::<TestData>(file_path) {
            Ok(test_data) => {
                let mut percent_rank = PercentRank::<f64>::new(test_data.period);
                let mut result: Vec<f64> = Vec::with_capacity(test_data.expected.len());

                for value in test_data.input.iter() {
                    result.push(percent_rank.next(*value))
                }

                assert_eq!(result, test_data.expected);
                // assert!(assert_vec_nearly_equal(
                //     &result,
                //     &test_data.expected,
                //     0.00000000001
                // ));
                Ok(())
            }
            Err(e) => {
                eprintln!("An error occurred: {:?}", e);
                Err(e)
            }
        }
    }
}
