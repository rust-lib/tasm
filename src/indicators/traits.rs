pub trait IndicatorTraits<T, U> {
    fn get_init_len(&self) -> usize;
    fn next(&mut self, inputs: T) -> U;
    fn live(&self, inputs: T) -> U;
}
