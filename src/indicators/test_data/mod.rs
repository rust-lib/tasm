use serde::de::DeserializeOwned;
use std::fs::File;
use std::io::Error;
use std::path::Path;

pub fn get_test_data<T: DeserializeOwned>(file_path: &str) -> Result<T, Error> {
    let path = Path::new(file_path);
    let file = File::open(path)?;
    let test_data = serde_json::from_reader(file)?;
    Ok(test_data)
}
