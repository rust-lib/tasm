// TODO: Improve perf
use super::ema::EMA;
use super::sma::SMA;
use super::traits::IndicatorTraits;
use num_traits::{Float, NumAssign};
use std::fmt::Debug;

#[derive(Debug, Clone)]
pub struct PriceVolume<T: Float + NumAssign + Debug> {
    period: usize,
    volume_factor: T,
    ema: EMA<T>,
    sma: SMA<T>,
    last_price: T,
    last_volume: T,
}

impl<T: Float + NumAssign + Debug> PriceVolume<T> {
    pub fn new(period: usize, smooth: usize, volume_factor: T) -> Self {
        PriceVolume {
            period,
            volume_factor,
            ema: EMA::new(smooth),
            sma: SMA::new(period),
            last_price: T::zero(),
            last_volume: T::zero(),
        }
    }

    fn calc(&self, price: T, volume: T) -> T {
        let prev_price = if self.last_price == T::zero() {
            price
        } else {
            self.last_price
        };

        let prev_volume = if self.last_volume == T::zero() {
            volume
        } else {
            self.last_volume
        };

        let volume_change = volume / prev_volume;
        let min_vol = volume_change.min(T::from(2.0).unwrap()) * self.volume_factor;
        let max_min_vol = min_vol.max(T::one());

        let factor = if volume > prev_volume {
            max_min_vol
        } else {
            T::one()
        };

        let point = if price > prev_price {
            price / prev_price * factor
        } else {
            -(prev_price / price * factor)
        };

        point
    }
}

impl<T: Float + NumAssign + Debug> IndicatorTraits<(T, T), T> for PriceVolume<T> {
    fn get_init_len(&self) -> usize {
        self.period + self.ema.get_init_len() - 2
    }

    #[inline]
    fn next(&mut self, values: (T, T)) -> T {
        let price = values.0;
        let volume = values.1;
        let point = self.calc(price, volume);

        let next = self.ema.next(self.sma.next(point));

        self.last_price = price;
        self.last_volume = volume;

        next
    }

    fn live(&self, values: (T, T)) -> T {
        let price = values.0;
        let volume = values.1;
        let point = self.calc(price, volume);
        self.ema.live(self.sma.live(point))
    }
}
#[cfg(test)]
mod tests {
    use super::*;
    use crate::indicators::test_data::get_test_data;
    use crate::tools::test::assert_vec_nearly_equal;
    use serde::{Deserialize, Serialize};
    use std::io::Error;

    #[derive(Serialize, Deserialize, Debug)]
    struct TestData {
        period: usize,
        smooth: usize,
        volume_factor: f64,
        init_len: usize,
        input: Vec<(f64, f64)>,
        expected: Vec<f64>,
    }

    #[test]
    fn test_price_volume() -> Result<(), Error> {
        let file_path = "src/indicators/test_data/indicators/priceVolume.json";

        match get_test_data::<TestData>(file_path) {
            Ok(test_data) => {
                let mut price_volume = PriceVolume::<f64>::new(
                    test_data.period,
                    test_data.smooth,
                    test_data.volume_factor,
                );

                let mut result: Vec<f64> = Vec::with_capacity(test_data.expected.len());

                for values in test_data.input.iter() {
                    result.push(price_volume.next(*values))
                }

                // assert_eq!(result, test_data.expected);
                assert!(assert_vec_nearly_equal(
                    &result,
                    &test_data.expected,
                    0.00000000001
                ));

                Ok(())
            }
            Err(e) => {
                eprintln!("An error occurred: {:?}", e);
                Err(e)
            }
        }
    }
}
