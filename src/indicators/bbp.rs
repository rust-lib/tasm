// TODO: Improve perf
use super::bb::BB;
use super::traits::IndicatorTraits;
use num_traits::{Float, NumAssign};
use std::fmt::Debug;

#[derive(Debug, Clone)]
pub struct BBP<T: Float + NumAssign + Debug> {
    period: usize,
    bb: BB<T>,
}

impl<T: Float + NumAssign + Debug> BBP<T> {
    pub fn new(period: usize, deviation: T) -> Self {
        BBP {
            period,
            bb: BB::new(period, deviation),
        }
    }

    fn calc(&self, value: T, upper: T, lower: T) -> T {
        let bbp = (value - lower) / (upper - lower);
        if bbp.is_nan() {
            T::zero()
        } else {
            bbp
        }
    }
}

impl<T: Float + NumAssign + Debug> IndicatorTraits<T, T> for BBP<T> {
    fn get_init_len(&self) -> usize {
        self.period
    }

    #[inline]
    fn next(&mut self, value: T) -> T {
        let (_, upper, lower) = self.bb.next(value);
        self.calc(value, upper, lower)
    }

    fn live(&self, value: T) -> T {
        let (_, upper, lower) = self.bb.live(value);
        self.calc(value, upper, lower)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::indicators::test_data::get_test_data;
    use crate::tools::test::assert_vec_nearly_equal;
    use serde::{Deserialize, Serialize};
    use std::io::Error;

    #[derive(Serialize, Deserialize, Debug)]
    struct TestData {
        period: usize,
        deviation: f64,
        init_len: usize,
        input: Vec<f64>,
        expected: Vec<f64>,
    }

    #[test]
    fn test_bbp() -> Result<(), Error> {
        let file_path = "src/indicators/test_data/indicators/bbp.json";

        match get_test_data::<TestData>(file_path) {
            Ok(test_data) => {
                let mut bbp = BBP::<f64>::new(test_data.period, test_data.deviation);
                let mut result: Vec<f64> = Vec::with_capacity(test_data.expected.len());

                for values in test_data.input.iter() {
                    result.push(bbp.next(*values))
                }

                // assert_eq!(result, test_data.expected);
                assert!(assert_vec_nearly_equal(
                    &result,
                    &test_data.expected,
                    0.00000000001
                ));

                Ok(())
            }
            Err(e) => {
                eprintln!("An error occurred: {:?}", e);
                Err(e)
            }
        }
    }
}
