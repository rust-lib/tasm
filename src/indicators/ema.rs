use super::traits::IndicatorTraits;
use num_traits::{Float, NumAssign};

#[derive(Debug, Clone)]
pub struct EMA<T: Float + NumAssign> {
    period: usize,
    exponent: T,
    last: T,
}

impl<T: Float + NumAssign> EMA<T> {
    pub fn new(period: usize) -> Self {
        EMA {
            period,
            exponent: T::from(2.0).unwrap() / (T::from(period).unwrap() + T::one()),
            last: T::zero(),
        }
    }

    fn calc(&self, value: T) -> T {
        (value - self.last) * self.exponent
    }
}

impl<T: Float + NumAssign> IndicatorTraits<T, T> for EMA<T> {
    fn get_init_len(&self) -> usize {
        self.period
    }

    #[inline]
    fn next(&mut self, value: T) -> T {
        self.last += self.calc(value);

        self.last
    }

    fn live(&self, value: T) -> T {
        self.last + self.calc(value)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::indicators::test_data::get_test_data;
    use crate::tools::test::assert_vec_nearly_equal;
    use serde::{Deserialize, Serialize};
    use std::io::Error;

    #[derive(Serialize, Deserialize, Debug)]
    struct TestData {
        period: usize,
        init_len: usize,
        input: Vec<f64>,
        expected: Vec<f64>,
    }

    #[test]
    fn test_ema() -> Result<(), Error> {
        let file_path = "src/indicators/test_data/indicators/ema.json";

        match get_test_data::<TestData>(file_path) {
            Ok(test_data) => {
                let mut ema = EMA::<f64>::new(test_data.period);
                let mut result: Vec<f64> = Vec::with_capacity(test_data.expected.len());

                for value in test_data.input.iter() {
                    result.push(ema.next(*value))
                }

                // assert_eq!(result, test_data.expected);
                assert!(assert_vec_nearly_equal(
                    &result,
                    &test_data.expected,
                    0.00000000001
                ));

                Ok(())
            }
            Err(e) => {
                eprintln!("An error occurred: {:?}", e);
                Err(e)
            }
        }
    }
}
