use super::traits::IndicatorTraits;
use crate::tools::cache::Cache;
use num_traits::{Float, NumAssign};
use std::fmt::Debug;

#[derive(Debug, Clone)]
pub struct Change<T: Float + NumAssign + Debug> {
    period: usize,
    cache: Cache<T>,
}

impl<T: Float + NumAssign + Debug> Change<T> {
    pub fn new(period: usize) -> Self {
        Change {
            period,
            cache: Cache::new(period + 1),
        }
    }

    fn calc(&self, value: T, oldest_value: Option<&T>) -> T {
        if let Some(oldest) = oldest_value {
            value - *oldest
        } else {
            T::zero()
        }
    }
}

impl<T: Float + NumAssign + Debug> IndicatorTraits<T, T> for Change<T> {
    fn get_init_len(&self) -> usize {
        self.period
    }

    #[inline]
    fn next(&mut self, value: T) -> T {
        self.cache.push(value);
        let oldest_value = self.cache.front();
        self.calc(value, oldest_value)
    }

    fn live(&self, value: T) -> T {
        let oldest_value = self.cache.get(1);
        self.calc(value, oldest_value)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::indicators::test_data::get_test_data;
    use crate::tools::test::assert_vec_nearly_equal;
    use serde::{Deserialize, Serialize};
    use std::io::Error;

    #[derive(Serialize, Deserialize, Debug)]
    struct TestData {
        period: usize,
        init_len: usize,
        input: Vec<f64>,
        expected: Vec<f64>,
    }

    #[test]
    fn test_change() -> Result<(), Error> {
        let file_path = "src/indicators/test_data/indicators/change.json";

        match get_test_data::<TestData>(file_path) {
            Ok(test_data) => {
                let mut change = Change::<f64>::new(test_data.period);
                let mut result: Vec<f64> = Vec::with_capacity(test_data.expected.len());

                for values in test_data.input.iter() {
                    result.push(change.next(*values))
                }

                // assert_eq!(result, test_data.expected);
                assert!(assert_vec_nearly_equal(
                    &result,
                    &test_data.expected,
                    0.00000000001
                ));

                Ok(())
            }
            Err(e) => {
                eprintln!("An error occurred: {:?}", e);
                Err(e)
            }
        }
    }
}
