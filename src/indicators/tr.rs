use super::traits::IndicatorTraits;
use num_traits::{Float, NumAssign};

#[derive(Debug, Clone)]
pub struct TR<T: Float + NumAssign> {
    last_close: T,
    t_0: T,
}

impl<T: Float + NumAssign> TR<T> {
    pub fn new() -> Self {
        TR {
            last_close: T::zero(),
            t_0: T::zero(),
        }
    }

    fn calc(&self, high: T, low: T) -> T {
        let mut tr = high - low;

        if self.last_close > self.t_0 {
            let hc = (high - self.last_close).abs();
            let lc = (low - self.last_close).abs();

            if hc > lc && hc > tr {
                tr = hc
            }

            if lc > hc && lc > tr {
                tr = lc
            }
        }

        tr
    }
}

impl<T: Float + NumAssign> IndicatorTraits<(T, T, T), T> for TR<T> {
    fn get_init_len(&self) -> usize {
        1
    }

    #[inline]
    fn next(&mut self, values: (T, T, T)) -> T {
        let high = values.0;
        let low = values.1;
        let close = values.2;
        let next = self.calc(high, low);
        self.last_close = close;

        next
    }

    fn live(&self, values: (T, T, T)) -> T {
        let high = values.0;
        let low = values.1;
        self.calc(high, low)
    }
}
#[cfg(test)]
mod tests {
    use super::*;
    use crate::indicators::test_data::get_test_data;
    use crate::tools::test::assert_vec_nearly_equal;
    use serde::{Deserialize, Serialize};
    use std::io::Error;

    #[derive(Serialize, Deserialize, Debug)]
    struct TestData {
        init_len: usize,
        input: Vec<(f64, f64, f64)>,
        expected: Vec<f64>,
    }

    #[test]
    fn test_tr() -> Result<(), Error> {
        let file_path = "src/indicators/test_data/indicators/tr.json";

        match get_test_data::<TestData>(file_path) {
            Ok(test_data) => {
                let mut tr = TR::<f64>::new();
                let mut result: Vec<f64> = Vec::with_capacity(test_data.expected.len());

                for values in test_data.input.iter() {
                    result.push(tr.next(*values))
                }

                // assert_eq!(result, test_data.expected);
                assert!(assert_vec_nearly_equal(
                    &result,
                    &test_data.expected,
                    0.00000000001
                ));

                Ok(())
            }
            Err(e) => {
                eprintln!("An error occurred: {:?}", e);
                Err(e)
            }
        }
    }
}
