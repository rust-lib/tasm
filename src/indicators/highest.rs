// TODO: Improve perf
use super::traits::IndicatorTraits;
use crate::tools::cache::Cache;
use num_traits::{Float, NumAssign};
use std::fmt::Debug;

#[derive(Debug, Clone)]
pub struct Highest<T: Float + NumAssign + Debug> {
    period: usize,
    cache: Cache<T>,
}

impl<T: Float + NumAssign + Debug> Highest<T> {
    pub fn new(period: usize) -> Self {
        Highest {
            period,
            cache: Cache::new(period),
        }
    }

    fn calc(&self, cache: &Cache<T>) -> (T, usize) {
        let (index, value) = cache
            .iter()
            .enumerate()
            .max_by(|a, b| a.1.partial_cmp(b.1).unwrap())
            .unwrap();
        (*value, index)
    }
}

impl<T: Float + NumAssign + Debug> IndicatorTraits<T, (T, usize)> for Highest<T> {
    fn get_init_len(&self) -> usize {
        self.period
    }

    #[inline]
    fn next(&mut self, value: T) -> (T, usize) {
        self.cache.push(value);
        self.calc(&self.cache)
    }

    fn live(&self, value: T) -> (T, usize) {
        let mut cache = self.cache.clone();
        cache.push(value);
        self.calc(&cache)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::indicators::test_data::get_test_data;
    // use crate::tools::test::assert_tuples2_nearly_equal;
    use serde::{Deserialize, Serialize};
    use std::io::Error;

    #[derive(Serialize, Deserialize, Debug)]
    struct TestData {
        period: usize,
        init_len: usize,
        input: Vec<f64>,
        expected: Vec<(f64, usize)>,
    }

    #[test]
    fn test_highest() -> Result<(), Error> {
        let file_path = "src/indicators/test_data/indicators/highest.json";

        match get_test_data::<TestData>(file_path) {
            Ok(test_data) => {
                let mut highest = Highest::<f64>::new(test_data.period);
                let mut result: Vec<(f64, usize)> = Vec::with_capacity(test_data.expected.len());

                for values in test_data.input.iter() {
                    result.push(highest.next(*values))
                }

                assert_eq!(result, test_data.expected);
                // assert!(assert_tuples2_nearly_equal(
                //     &result,
                //     &test_data.expected,
                //     0.00000000001
                // ));

                Ok(())
            }
            Err(e) => {
                eprintln!("An error occurred: {:?}", e);
                Err(e)
            }
        }
    }
}
