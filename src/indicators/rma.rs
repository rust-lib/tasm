use super::traits::IndicatorTraits;
use num_traits::{Float, NumAssign};

#[derive(Debug, Clone)]
pub struct RMA<T: Float + NumAssign> {
    period: usize,
    exponent: T,
    last: T,
}

impl<T: Float + NumAssign> RMA<T> {
    pub fn new(period: usize) -> Self {
        RMA {
            period,
            exponent: T::one() / T::from(period).unwrap(),
            last: T::zero(),
        }
    }

    fn calc(&self, value: T, last: T) -> T {
        (value - last) * self.exponent
    }
}

impl<T: Float + NumAssign> IndicatorTraits<T, T> for RMA<T> {
    fn get_init_len(&self) -> usize {
        self.period
    }

    #[inline]
    fn next(&mut self, value: T) -> T {
        let next = self.calc(value, self.last);
        self.last += next;

        self.last
    }

    fn live(&self, value: T) -> T {
        self.calc(value, self.last)
    }
}
#[cfg(test)]
mod tests {
    use super::*;
    use crate::indicators::test_data::get_test_data;
    use crate::tools::test::assert_vec_nearly_equal;
    use serde::{Deserialize, Serialize};
    use std::io::Error;

    #[derive(Serialize, Deserialize, Debug)]
    struct TestData {
        period: usize,
        init_len: usize,
        input: Vec<f64>,
        expected: Vec<f64>,
    }

    #[test]
    fn test_rma() -> Result<(), Error> {
        let file_path = "src/indicators/test_data/indicators/rma.json";

        match get_test_data::<TestData>(file_path) {
            Ok(test_data) => {
                let mut rma = RMA::<f64>::new(test_data.period);
                let mut result: Vec<f64> = Vec::with_capacity(test_data.expected.len());

                for value in test_data.input.iter() {
                    result.push(rma.next(*value))
                }

                // assert_eq!(result, test_data.expected);
                assert!(assert_vec_nearly_equal(
                    &result,
                    &test_data.expected,
                    0.00000000001
                ));

                Ok(())
            }
            Err(e) => {
                // If there was an error, `e` will contain the error
                eprintln!("An error occurred: {:?}", e);
                Err(e)
            }
        }
    }
}
