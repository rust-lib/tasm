// TODO: Improve perf
use super::traits::IndicatorTraits;
use num_traits::{Float, NumAssign};
use std::fmt::Debug;

#[derive(Debug, Clone)]
pub struct Indicator<T: Float + NumAssign + Debug> {
    period: usize,
}

impl<T: Float + NumAssign + Debug> Indicator<T> {
    pub fn new(period: usize) -> Self {
        Indicator { period }
    }
}

impl<T: Float + NumAssign + Debug> IndicatorTraits<T, T> for Indicator<T> {
    fn get_init_len(&self) -> usize {
        self.period
    }

    #[inline]
    fn next(&mut self, value: T) -> T {}

    fn live(&self, value: T) -> T {}
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::indicators::test_data::get_test_data;
    use crate::tools::test::assert_vec_nearly_equal;
    use serde::{Deserialize, Serialize};
    use std::io::Error;

    #[derive(Serialize, Deserialize, Debug)]
    struct TestData {
        period: usize,
        init_len: usize,
        input: Vec<f64>,
        expected: Vec<f64>,
    }

    #[test]
    fn test_hma() -> Result<(), Error> {
        let file_path = "src/indicators/test_data/indicators/indicator.json";

        match get_test_data::<TestData>(file_path) {
            Ok(test_data) => {
                let mut indicator = Indicator::<f64>::new(test_data.period);
                let mut result: Vec<f64> = Vec::with_capacity(test_data.expected.len());

                for values in test_data.input.iter() {
                    result.push(indicator.next(*values))
                }

                assert_eq!(result, test_data.expected);
                assert!(assert_vec_nearly_equal(
                    &result,
                    &test_data.expected,
                    0.00000000001
                ));

                Ok(())
            }
            Err(e) => {
                eprintln!("An error occurred: {:?}", e);
                Err(e)
            }
        }
    }
}
