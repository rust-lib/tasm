// TODO: Improve perf
use super::rsi::RSI;
use super::traits::IndicatorTraits;
use super::vwma::VWMA;
use num_traits::{Float, NumAssign};
use std::fmt::Debug;

#[derive(Debug, Clone)]
pub struct SVO<T: Float + NumAssign + Debug> {
    vwma: VWMA<T>,
    rsi: RSI<T>,
}

impl<T: Float + NumAssign + Debug> SVO<T> {
    pub fn new(period: usize, smooth: usize) -> Self {
        SVO {
            vwma: VWMA::new(smooth),
            rsi: RSI::new(period),
        }
    }
}

impl<T: Float + NumAssign + Debug> IndicatorTraits<T, T> for SVO<T> {
    fn get_init_len(&self) -> usize {
        self.vwma.get_init_len() + self.rsi.get_init_len()
    }

    #[inline]
    fn next(&mut self, value: T) -> T {
        let vwma = self.vwma.next((value, value));
        self.rsi.next(vwma)
    }

    fn live(&self, value: T) -> T {
        let vwma = self.vwma.live((value, value));
        self.rsi.live(vwma)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::indicators::test_data::get_test_data;
    use crate::tools::test::assert_vec_nearly_equal;
    use serde::{Deserialize, Serialize};
    use std::io::Error;

    #[derive(Serialize, Deserialize, Debug)]
    struct TestData {
        period: usize,
        smooth: usize,
        init_len: usize,
        input: Vec<f64>,
        expected: Vec<f64>,
    }

    #[test]
    fn test_svo() -> Result<(), Error> {
        let file_path = "src/indicators/test_data/indicators/svo.json";

        match get_test_data::<TestData>(file_path) {
            Ok(test_data) => {
                let mut svo = SVO::<f64>::new(test_data.period, test_data.smooth);
                let mut result: Vec<f64> = Vec::with_capacity(test_data.expected.len());

                for values in test_data.input.iter() {
                    result.push(svo.next(*values))
                }

                // assert_eq!(result, test_data.expected);
                assert!(assert_vec_nearly_equal(
                    &result,
                    &test_data.expected,
                    0.00000000001
                ));

                Ok(())
            }
            Err(e) => {
                eprintln!("An error occurred: {:?}", e);
                Err(e)
            }
        }
    }
}
