// TODO: Improve perf
use super::traits::IndicatorTraits;
use crate::tools::cache::Cache;
use num_traits::{Float, NumAssign};
use std::fmt::Debug;

#[derive(Debug, Clone)]
pub struct WMA<T: Float + NumAssign + Debug> {
    period: usize,
    cache: Cache<T>,
}

impl<T: Float + NumAssign + Debug> WMA<T> {
    pub fn new(period: usize) -> Self {
        WMA {
            period,
            cache: Cache::new(period),
        }
    }

    fn calc(&self, cache: &Cache<T>) -> T {
        let mut norm = T::zero();
        let mut sum = T::zero();

        for (index, value) in cache.iter().enumerate() {
            let weight = (T::from(index).unwrap() + T::one()) * T::from(self.period).unwrap();
            norm += weight;
            sum += *value * weight;
        }

        sum / norm
    }
}

impl<T: Float + NumAssign + Debug> IndicatorTraits<T, T> for WMA<T> {
    fn get_init_len(&self) -> usize {
        self.period
    }

    #[inline]
    fn next(&mut self, value: T) -> T {
        self.cache.push(value);
        self.calc(&self.cache)
    }

    fn live(&self, value: T) -> T {
        let mut cache = self.cache.clone();
        cache.push(value);
        self.calc(&cache)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::indicators::test_data::get_test_data;
    use crate::tools::test::assert_vec_nearly_equal;
    use serde::{Deserialize, Serialize};
    use std::io::Error;

    #[derive(Serialize, Deserialize, Debug)]
    struct TestData {
        period: usize,
        init_len: usize,
        input: Vec<f64>,
        expected: Vec<f64>,
    }

    #[test]
    fn test_wma() -> Result<(), Error> {
        let file_path = "src/indicators/test_data/indicators/wma.json";

        match get_test_data::<TestData>(file_path) {
            Ok(test_data) => {
                let mut wma = WMA::<f64>::new(test_data.period);
                let mut result: Vec<f64> = Vec::with_capacity(test_data.expected.len());

                for values in test_data.input.iter() {
                    result.push(wma.next(*values))
                }

                // assert_eq!(result, test_data.expected);
                assert!(assert_vec_nearly_equal(
                    &result,
                    &test_data.expected,
                    0.00000000001
                ));

                Ok(())
            }
            Err(e) => {
                eprintln!("An error occurred: {:?}", e);
                Err(e)
            }
        }
    }
}
