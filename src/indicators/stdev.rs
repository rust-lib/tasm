// TODO: Improve perf
use super::sma::SMA;
use super::traits::IndicatorTraits;
use crate::tools::cache::Cache;
use num_traits::{Float, NumAssign};
use std::fmt::Debug;

#[derive(Debug, Clone)]
pub struct StDev<T: Float + NumAssign + Debug> {
    period: usize,
    sma: SMA<T>,
    cache: Cache<T>,
    t_2: T,
}

impl<T: Float + NumAssign + Debug> StDev<T> {
    pub fn new(period: usize) -> Self {
        StDev {
            period,
            sma: SMA::new(period),
            cache: Cache::new(period),
            t_2: T::from(2).unwrap(),
        }
    }

    fn calc(&self, sma: T) -> T {
        let mut sum = T::zero();
        // check if it can be optimized using the same method of SMA
        for val in self.cache.iter() {
            sum += (*val - sma).powf(self.t_2);
        }

        let avg = sum / T::from(self.cache.len()).unwrap();

        avg.sqrt()
    }
}

impl<T: Float + NumAssign + Debug> IndicatorTraits<T, T> for StDev<T> {
    fn get_init_len(&self) -> usize {
        self.period
    }

    #[inline]
    fn next(&mut self, value: T) -> T {
        self.cache.push(value);
        let sma = self.sma.next(value);
        self.calc(sma)
    }

    fn live(&self, value: T) -> T {
        let sma = self.sma.live(value);
        self.calc(sma)
    }
}
#[cfg(test)]
mod tests {
    use super::*;
    use crate::indicators::test_data::get_test_data;
    use crate::tools::test::assert_vec_nearly_equal;
    use serde::{Deserialize, Serialize};
    use std::io::Error;

    #[derive(Serialize, Deserialize, Debug)]
    struct TestData {
        period: usize,
        init_len: usize,
        input: Vec<f64>,
        expected: Vec<f64>,
    }

    #[test]
    fn test_stdev() -> Result<(), Error> {
        let file_path = "src/indicators/test_data/indicators/stdev.json";

        match get_test_data::<TestData>(file_path) {
            Ok(test_data) => {
                let mut stdev = StDev::<f64>::new(test_data.period);
                let mut result: Vec<f64> = Vec::with_capacity(test_data.expected.len());

                for values in test_data.input.iter() {
                    result.push(stdev.next(*values))
                }

                // assert_eq!(result, test_data.expected);
                assert!(assert_vec_nearly_equal(
                    &result,
                    &test_data.expected,
                    0.00000000001
                ));

                Ok(())
            }
            Err(e) => {
                eprintln!("An error occurred: {:?}", e);
                Err(e)
            }
        }
    }
}
