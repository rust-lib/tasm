// TODO: Improve perf
use super::sma::SMA;
use super::traits::IndicatorTraits;
use num_traits::{Float, NumAssign};
use std::fmt::Debug;

#[derive(Debug, Clone)]
pub struct VWMA<T: Float + NumAssign + Debug> {
    period: usize,
    sma_pv: SMA<T>,
    sma_v: SMA<T>,
}

impl<T: Float + NumAssign + Debug> VWMA<T> {
    pub fn new(period: usize) -> Self {
        VWMA {
            period,
            sma_pv: SMA::new(period),
            sma_v: SMA::new(period),
        }
    }
}

impl<T: Float + NumAssign + Debug> IndicatorTraits<(T, T), T> for VWMA<T> {
    fn get_init_len(&self) -> usize {
        self.period
    }

    #[inline]
    fn next(&mut self, values: (T, T)) -> T {
        let close = values.0;
        let volume = values.1;
        let sma_pv = self.sma_pv.next(close * volume);
        let sma_v = self.sma_v.next(volume);
        sma_pv / sma_v
    }

    fn live(&self, values: (T, T)) -> T {
        let close = values.0;
        let volume = values.1;
        let sma_pv = self.sma_pv.live(close * volume);
        let sma_v = self.sma_v.live(volume);
        sma_pv / sma_v
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::indicators::test_data::get_test_data;
    use crate::tools::test::assert_vec_nearly_equal;
    use serde::{Deserialize, Serialize};
    use std::io::Error;

    #[derive(Serialize, Deserialize, Debug)]
    struct TestData {
        period: usize,
        init_len: usize,
        input: Vec<(f64, f64)>,
        expected: Vec<f64>,
    }

    #[test]
    fn test_vwma() -> Result<(), Error> {
        let file_path = "src/indicators/test_data/indicators/vwma.json";

        match get_test_data::<TestData>(file_path) {
            Ok(test_data) => {
                let mut vwma = VWMA::<f64>::new(test_data.period);
                let mut result: Vec<f64> = Vec::with_capacity(test_data.expected.len());

                for values in test_data.input.iter() {
                    result.push(vwma.next(*values))
                }

                // assert_eq!(result, test_data.expected);
                assert!(assert_vec_nearly_equal(
                    &result,
                    &test_data.expected,
                    0.00000000001
                ));

                Ok(())
            }
            Err(e) => {
                eprintln!("An error occurred: {:?}", e);
                Err(e)
            }
        }
    }
}
