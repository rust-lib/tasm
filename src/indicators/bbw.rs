// TODO: Improve perf
use super::bb::BB;
use super::traits::IndicatorTraits;
use num_traits::{Float, NumAssign};
use std::fmt::Debug;

#[derive(Debug, Clone)]
pub struct BBW<T: Float + NumAssign + Debug> {
    period: usize,
    bb: BB<T>,
    t_0: T,
}

impl<T: Float + NumAssign + Debug> BBW<T> {
    pub fn new(period: usize, deviation: T) -> Self {
        BBW {
            period,
            bb: BB::new(period, deviation),
            t_0: T::zero(),
        }
    }

    fn calc(&self, basis: T, upper: T, lower: T) -> T {
        let bbw = (upper - lower) / basis;
        if bbw.is_nan() {
            self.t_0
        } else {
            bbw
        }
    }
}

impl<T: Float + NumAssign + Debug> IndicatorTraits<T, T> for BBW<T> {
    fn get_init_len(&self) -> usize {
        self.period
    }

    #[inline]
    fn next(&mut self, value: T) -> T {
        let (basis, upper, lower) = self.bb.next(value);
        self.calc(basis, upper, lower)
    }

    fn live(&self, value: T) -> T {
        let (basis, upper, lower) = self.bb.live(value);
        self.calc(basis, upper, lower)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::indicators::test_data::get_test_data;
    use crate::tools::test::assert_vec_nearly_equal;
    use serde::{Deserialize, Serialize};
    use std::io::Error;

    #[derive(Serialize, Deserialize, Debug)]
    struct TestData {
        period: usize,
        deviation: f64,
        init_len: usize,
        input: Vec<f64>,
        expected: Vec<f64>,
    }

    #[test]
    fn test_bbw() -> Result<(), Error> {
        let file_path = "src/indicators/test_data/indicators/bbw.json";

        match get_test_data::<TestData>(file_path) {
            Ok(test_data) => {
                let mut bbw = BBW::<f64>::new(test_data.period, test_data.deviation);
                let mut result: Vec<f64> = Vec::with_capacity(test_data.expected.len());

                for values in test_data.input.iter() {
                    result.push(bbw.next(*values))
                }

                // assert_eq!(result, test_data.expected);
                assert!(assert_vec_nearly_equal(
                    &result,
                    &test_data.expected,
                    0.00000000001
                ));

                Ok(())
            }
            Err(e) => {
                eprintln!("An error occurred: {:?}", e);
                Err(e)
            }
        }
    }
}
