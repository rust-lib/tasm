// TODO: Improve perf
use super::highest::Highest;
use super::lowest::Lowest;
use super::sma::SMA;
use super::stdev::StDev;
use super::traits::IndicatorTraits;
use num_traits::{Float, NumAssign};
use std::fmt::Debug;

#[derive(Debug, Clone)]
pub struct PAI<T: Float + NumAssign + Debug> {
    period: usize,
    stdev: StDev<T>,
    highest: Highest<T>,
    lowest: Lowest<T>,
    highest_stdev: Highest<T>,
    lowest_stdev: Lowest<T>,
    sma: SMA<T>,
    t_50: T,
    t_100: T,
}

impl<T: Float + NumAssign + Debug> PAI<T> {
    pub fn new(period: usize, smooth: usize, dispersion: usize) -> Self {
        PAI {
            period,
            stdev: StDev::new(dispersion),
            highest: Highest::new(period),
            lowest: Lowest::new(period),
            highest_stdev: Highest::new(period),
            lowest_stdev: Lowest::new(period),
            sma: SMA::new(smooth),
            t_50: T::from(50).unwrap(),
            t_100: T::from(100).unwrap(),
        }
    }

    fn stoch_k(&self, current: T, highest: T, lowest: T) -> T {
        ((current - lowest) / (highest - lowest)) * self.t_100
    }
}

impl<T: Float + NumAssign + Debug> IndicatorTraits<(T, T, T), T> for PAI<T> {
    fn get_init_len(&self) -> usize {
        self.period
    }

    #[inline]
    fn next(&mut self, values: (T, T, T)) -> T {
        let high = values.0;
        let low = values.1;
        let close = values.2;
        let stdev = self.stdev.next(close);
        let highest = self.highest.next(high);
        let lowest = self.lowest.next(low);
        let stoch_k = (self.stoch_k(close, highest.0, lowest.0) - self.t_50) / self.t_50;
        let smooth_stoch_k = self.sma.next(stoch_k);
        let highest_stdev = self.highest_stdev.next(stdev);
        let lowest_stdev = self.lowest_stdev.next(stdev);
        let stoch_stdev = self.stoch_k(stdev, highest_stdev.0, lowest_stdev.0);
        let pai = smooth_stoch_k * stoch_stdev;
        if pai.is_nan() {
            T::zero()
        } else {
            pai
        }
    }

    fn live(&self, values: (T, T, T)) -> T {
        let high = values.0;
        let low = values.1;
        let close = values.2;
        let stdev = self.stdev.live(close);
        let highest = self.highest.live(high);
        let lowest = self.lowest.live(low);
        let stoch_k = (self.stoch_k(close, highest.0, lowest.0) - self.t_50) / self.t_50;
        let smooth_stoch_k = self.sma.live(stoch_k);
        let highest_stdev = self.highest_stdev.live(stdev);
        let lowest_stdev = self.lowest_stdev.live(stdev);
        let stoch_stdev = self.stoch_k(stdev, highest_stdev.0, lowest_stdev.0);
        let pai = smooth_stoch_k * stoch_stdev;
        if pai.is_nan() {
            T::zero()
        } else {
            pai
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::indicators::test_data::get_test_data;
    use crate::tools::test::assert_vec_nearly_equal;
    use serde::{Deserialize, Serialize};
    use std::io::Error;

    #[derive(Serialize, Deserialize, Debug)]
    struct TestData {
        period: usize,
        smooth: usize,
        dispersion: usize,
        init_len: usize,
        input: Vec<(f64, f64, f64)>,
        expected: Vec<f64>,
    }

    #[test]
    fn test_pai() -> Result<(), Error> {
        let file_path = "src/indicators/test_data/indicators/pai.json";

        match get_test_data::<TestData>(file_path) {
            Ok(test_data) => {
                let mut pai =
                    PAI::<f64>::new(test_data.period, test_data.smooth, test_data.dispersion);
                let mut result: Vec<f64> = Vec::with_capacity(test_data.expected.len());

                for values in test_data.input.iter() {
                    result.push(pai.next(*values))
                }

                // assert_eq!(result, test_data.expected);
                assert!(assert_vec_nearly_equal(
                    &result,
                    &test_data.expected,
                    0.00000000001
                ));

                Ok(())
            }
            Err(e) => {
                eprintln!("An error occurred: {:?}", e);
                Err(e)
            }
        }
    }
}
