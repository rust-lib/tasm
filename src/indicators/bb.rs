// TODO: Improve perf
use super::sma::SMA;
use super::stdev::StDev;
use super::traits::IndicatorTraits;
use num_traits::{Float, NumAssign};
use std::fmt::Debug;

#[derive(Debug, Clone)]
pub struct BB<T: Float + NumAssign + Debug> {
    period: usize,
    deviation: T,
    sma: SMA<T>,
    stdev: StDev<T>,
}

impl<T: Float + NumAssign + Debug> BB<T> {
    pub fn new(period: usize, deviation: T) -> Self {
        BB {
            period,
            deviation,
            sma: SMA::new(period),
            stdev: StDev::new(period),
        }
    }

    fn calc(&self, sma: T, stdev: T) -> (T, T, T) {
        let dev = stdev * self.deviation;
        let upper = sma + dev;
        let lower = sma - dev;
        (sma, upper, lower)
    }
}

impl<T: Float + NumAssign + Debug> IndicatorTraits<T, (T, T, T)> for BB<T> {
    fn get_init_len(&self) -> usize {
        self.period
    }

    #[inline]
    fn next(&mut self, value: T) -> (T, T, T) {
        let sma = self.sma.next(value);
        let stdev = self.stdev.next(value);
        self.calc(sma, stdev)
    }

    fn live(&self, value: T) -> (T, T, T) {
        let sma = self.sma.live(value);
        let stdev = self.stdev.live(value);
        self.calc(sma, stdev)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::indicators::test_data::get_test_data;
    use crate::tools::test::assert_tuples3_nearly_equal;
    use serde::{Deserialize, Serialize};
    use std::io::Error;

    #[derive(Serialize, Deserialize, Debug)]
    struct TestData {
        period: usize,
        deviation: f64,
        init_len: usize,
        input: Vec<f64>,
        expected: Vec<(f64, f64, f64)>,
    }

    #[test]
    fn test_bb() -> Result<(), Error> {
        let file_path = "src/indicators/test_data/indicators/bb.json";

        match get_test_data::<TestData>(file_path) {
            Ok(test_data) => {
                let mut bb = BB::<f64>::new(test_data.period, test_data.deviation);
                let mut result: Vec<(f64, f64, f64)> = Vec::with_capacity(test_data.expected.len());

                for values in test_data.input.iter() {
                    result.push(bb.next(*values))
                }

                // assert_eq!(result, test_data.expected);
                assert!(assert_tuples3_nearly_equal(
                    &result,
                    &test_data.expected,
                    0.00000000001
                ));

                Ok(())
            }
            Err(e) => {
                eprintln!("An error occurred: {:?}", e);
                Err(e)
            }
        }
    }
}
