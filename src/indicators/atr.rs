use super::rma::RMA;
use super::tr::TR;
use super::traits::IndicatorTraits;
use num_traits::{Float, NumAssign};

#[derive(Debug, Clone)]
pub struct ATR<T: Float + NumAssign> {
    tr: TR<T>,
    rma: RMA<T>,
    last_close: T,
}

impl<T: Float + NumAssign> ATR<T> {
    pub fn new(period: usize) -> Self {
        ATR {
            tr: TR::new(),
            rma: RMA::new(period),
            last_close: T::zero(),
        }
    }
}

impl<T: Float + NumAssign> IndicatorTraits<(T, T, T), T> for ATR<T> {
    fn get_init_len(&self) -> usize {
        1
    }

    #[inline]
    fn next(&mut self, values: (T, T, T)) -> T {
        let high = values.0;
        let low = values.1;
        let close = values.2;
        let tr = self.tr.next((high, low, close));
        let next = self.rma.next(tr);
        self.last_close = close;

        next
    }

    fn live(&self, values: (T, T, T)) -> T {
        let high = values.0;
        let low = values.1;
        let close = values.2;
        let tr = self.tr.live((high, low, close));
        self.rma.live(tr)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::indicators::test_data::get_test_data;
    use crate::tools::test::assert_vec_nearly_equal;
    use serde::{Deserialize, Serialize};
    use std::io::Error;

    #[derive(Serialize, Deserialize, Debug)]
    struct TestData {
        period: usize,
        init_len: usize,
        input: Vec<(f64, f64, f64)>,
        expected: Vec<f64>,
    }

    #[test]
    fn test_atr() -> Result<(), Error> {
        let file_path = "src/indicators/test_data/indicators/atr.json";

        match get_test_data::<TestData>(file_path) {
            Ok(test_data) => {
                let mut atr = ATR::<f64>::new(test_data.period);
                let mut result: Vec<f64> = Vec::with_capacity(test_data.expected.len());

                for values in test_data.input.iter() {
                    result.push(atr.next(*values))
                }

                // assert_eq!(result, test_data.expected);
                assert!(assert_vec_nearly_equal(
                    &result,
                    &test_data.expected,
                    0.00000000001
                ));

                Ok(())
            }
            Err(e) => {
                eprintln!("An error occurred: {:?}", e);
                Err(e)
            }
        }
    }
}
