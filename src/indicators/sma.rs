// TODO: Improve perf
use super::traits::IndicatorTraits;
use crate::tools::cache::Cache;
use num_traits::{Float, NumAssign};
use std::fmt::Debug;

#[derive(Debug, Clone)]
pub struct SMA<T: Float + NumAssign + Debug> {
    period: usize,
    cache: Cache<T>,
    sum: T,
}

impl<T: Float + NumAssign + Debug> SMA<T> {
    pub fn new(period: usize) -> Self {
        SMA {
            period,
            cache: Cache::new(period),
            sum: T::zero(),
        }
    }

    fn calc(&self, value: T, shifted_value: Option<T>) -> T {
        let mut sum = self.sum + value;

        if let Some(old_value) = shifted_value {
            sum -= old_value;
        }

        sum
    }
}

impl<T: Float + NumAssign + Debug> IndicatorTraits<T, T> for SMA<T> {
    fn get_init_len(&self) -> usize {
        self.period
    }

    #[inline]
    fn next(&mut self, value: T) -> T {
        let shifted_value = self.cache.push(value);
        self.sum = self.calc(value, shifted_value);

        self.sum / T::from(self.cache.len()).unwrap()
    }

    fn live(&self, value: T) -> T {
        let mut cache = self.cache.clone();
        let shifted_value = cache.push(value);
        self.calc(value, shifted_value) / T::from(cache.len()).unwrap()
    }
}
#[cfg(test)]
mod tests {
    use super::*;
    use crate::indicators::test_data::get_test_data;
    use crate::tools::test::assert_vec_nearly_equal;
    use serde::{Deserialize, Serialize};
    use std::io::Error;

    #[derive(Serialize, Deserialize, Debug)]
    struct TestData {
        period: usize,
        init_len: usize,
        input: Vec<f64>,
        expected: Vec<f64>,
    }

    #[test]
    fn test_sma() -> Result<(), Error> {
        let file_path = "src/indicators/test_data/indicators/sma.json";

        match get_test_data::<TestData>(file_path) {
            Ok(test_data) => {
                let mut sma = SMA::<f64>::new(test_data.period);
                let mut result: Vec<f64> = Vec::with_capacity(test_data.expected.len());

                for value in test_data.input.iter() {
                    result.push(sma.next(*value))
                }

                // assert_eq!(result, test_data.expected);
                assert!(assert_vec_nearly_equal(
                    &result,
                    &test_data.expected,
                    0.00000000001
                ));
                Ok(())
            }
            Err(e) => {
                eprintln!("An error occurred: {:?}", e);
                Err(e)
            }
        }
    }
}
