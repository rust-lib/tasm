// TODO: Improve perf
use super::traits::IndicatorTraits;
use crate::tools::cache::Cache;
use num_traits::{Float, NumAssign};
use std::fmt::Debug;

#[derive(Debug, Clone)]
pub struct PSAR<T: Float + NumAssign + Debug> {
    start: T,
    step: T,
    max: T,
    cache_high: Cache<T>,
    cache_low: Cache<T>,
    extreme: T,
    up: bool,
    accel: T,
    psar: T,
}

impl<T: Float + NumAssign + Debug> PSAR<T> {
    pub fn new(start: T, step: T, max: T) -> Self {
        PSAR {
            start,
            step,
            max,
            cache_high: Cache::new(2),
            cache_low: Cache::new(2),
            extreme: T::zero(),
            up: true,
            accel: start,
            psar: T::zero(),
        }
    }

    fn calc(&self, high: T, low: T) -> (T, T, T, bool) {
        let mut accel = self.accel;
        let mut extreme = self.extreme;
        let mut up = self.up;
        let mut psar = self.psar + accel * (extreme - self.psar);
        let h2 = self.cache_high.front().unwrap_or(&high);
        let h1 = self.cache_high.back().unwrap_or(&high);
        let l2 = self.cache_low.front().unwrap_or(&low);
        let l1 = self.cache_low.back().unwrap_or(&low);

        if up {
            if l1 < &psar || l2 < &psar {
                psar = if l1 < l2 { *l1 } else { *l2 }
            }
            if high > extreme {
                extreme = high;
                let next_accel = accel + self.step;
                accel = if next_accel > self.max {
                    self.max
                } else {
                    next_accel
                };
            };
        } else {
            if h1 > &psar || h2 > &psar {
                psar = if h1 > h2 { *h1 } else { *h2 }
            }
            if low < extreme {
                extreme = low;
                let next_accel = accel + self.step;
                accel = if next_accel > self.max {
                    self.max
                } else {
                    next_accel
                };
            };
        };

        if (up && low < psar) || (!up && high > psar) {
            accel = self.start;
            psar = extreme;
            if up {
                up = false;
                extreme = low;
            } else {
                up = true;
                extreme = high
            };
        };

        (psar, accel, extreme, up)
    }
}

impl<T: Float + NumAssign + Debug> IndicatorTraits<(T, T), T> for PSAR<T> {
    fn get_init_len(&self) -> usize {
        60
    }

    #[inline]
    fn next(&mut self, values: (T, T)) -> T {
        let high = values.0;
        let low = values.1;

        let res = self.calc(high, low);
        self.psar = res.0;
        self.accel = res.1;
        self.extreme = res.2;
        self.up = res.3;
        self.cache_high.push(high);
        self.cache_low.push(low);
        self.psar
    }

    fn live(&self, values: (T, T)) -> T {
        let high = values.0;
        let low = values.1;
        let res = self.calc(high, low);
        res.0
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::indicators::test_data::get_test_data;
    use crate::tools::test::assert_vec_nearly_equal;
    use serde::{Deserialize, Serialize};
    use std::io::Error;

    #[derive(Serialize, Deserialize, Debug)]
    struct TestData {
        start: f64,
        step: f64,
        max: f64,
        init_len: usize,
        input: Vec<(f64, f64)>,
        expected: Vec<f64>,
    }

    #[test]
    fn test_psar() -> Result<(), Error> {
        let file_path = "src/indicators/test_data/indicators/psar.json";

        match get_test_data::<TestData>(file_path) {
            Ok(test_data) => {
                let mut psar = PSAR::<f64>::new(test_data.start, test_data.step, test_data.max);
                let mut result: Vec<f64> = Vec::with_capacity(test_data.expected.len());

                for values in test_data.input.iter() {
                    result.push(psar.next(*values))
                }

                // assert_eq!(result, test_data.expected);
                assert!(assert_vec_nearly_equal(
                    &result,
                    &test_data.expected,
                    0.00000000001
                ));

                Ok(())
            }
            Err(e) => {
                eprintln!("An error occurred: {:?}", e);
                Err(e)
            }
        }
    }
}
