// TODO: Improve perf
use super::traits::IndicatorTraits;
use super::wma::WMA;
use num_traits::{Float, NumAssign};
use std::fmt::Debug;

#[derive(Debug, Clone)]
pub struct HMA<T: Float + NumAssign + Debug> {
    period: usize,
    wma1: WMA<T>,
    wma2: WMA<T>,
    wma3: WMA<T>,
    t_2: T,
}

impl<T: Float + NumAssign + Debug> HMA<T> {
    pub fn new(period: usize) -> Self {
        HMA {
            period,
            wma1: WMA::new(period),
            wma2: WMA::new(period / 2),
            wma3: WMA::new((period as f64).sqrt() as usize),
            t_2: T::from(2).unwrap(),
        }
    }
}

impl<T: Float + NumAssign + Debug> IndicatorTraits<T, T> for HMA<T> {
    fn get_init_len(&self) -> usize {
        self.period
    }

    #[inline]
    fn next(&mut self, value: T) -> T {
        let wma1 = self.wma1.next(value);
        let wma2 = self.wma2.next(value);
        let raw_hma = self.t_2 * wma2 - wma1;
        self.wma3.next(raw_hma)
    }

    fn live(&self, value: T) -> T {
        let wma1 = self.wma1.live(value);
        let wma2 = self.wma2.live(value);
        let raw_hma = self.t_2 * wma2 - wma1;
        self.wma3.live(raw_hma)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::indicators::test_data::get_test_data;
    // use crate::tools::test::assert_vec_nearly_equal;
    use serde::{Deserialize, Serialize};
    use std::io::Error;

    #[derive(Serialize, Deserialize, Debug)]
    struct TestData {
        period: usize,
        init_len: usize,
        input: Vec<f64>,
        expected: Vec<f64>,
    }

    #[test]
    fn test_hma() -> Result<(), Error> {
        let file_path = "src/indicators/test_data/indicators/hma.json";

        match get_test_data::<TestData>(file_path) {
            Ok(test_data) => {
                let mut hma = HMA::<f64>::new(test_data.period);
                let mut result: Vec<f64> = Vec::with_capacity(test_data.expected.len());

                for values in test_data.input.iter() {
                    result.push(hma.next(*values))
                }

                assert_eq!(result, test_data.expected);
                // assert!(assert_vec_nearly_equal(
                //     &result,
                //     &test_data.expected,
                //     0.00000000001
                // ));

                Ok(())
            }
            Err(e) => {
                eprintln!("An error occurred: {:?}", e);
                Err(e)
            }
        }
    }
}
