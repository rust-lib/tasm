use super::traits::IndicatorTraits;
use num_traits::{Float, NumAssign};

#[derive(Debug, Clone)]
pub struct EWMA<T: Float + NumAssign> {
    period: usize,
    lambda: T,
    last_weight: T,
    t_2: T,
}

impl<T: Float + NumAssign> EWMA<T> {
    pub fn new(period: usize) -> Self {
        EWMA {
            period,
            lambda: (T::from(period).unwrap() - T::one()) / (T::from(period).unwrap() + T::one()),
            last_weight: T::zero(),
            t_2: T::from(2).unwrap(),
        }
    }

    fn calc(&self, value: T) -> (T, T) {
        let squared = value.powf(self.t_2);
        let weight = self.lambda * self.last_weight + (T::one() - self.lambda) * squared;
        (weight.sqrt(), weight)
    }
}

impl<T: Float + NumAssign> IndicatorTraits<T, T> for EWMA<T> {
    fn get_init_len(&self) -> usize {
        self.period
    }

    #[inline]
    fn next(&mut self, value: T) -> T {
        let (ewma, last_weight) = self.calc(value);
        self.last_weight = last_weight;
        ewma
    }

    fn live(&self, value: T) -> T {
        let (ewma, _) = self.calc(value);
        ewma
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::indicators::test_data::get_test_data;
    use crate::tools::test::assert_vec_nearly_equal;
    use serde::{Deserialize, Serialize};
    use std::io::Error;

    #[derive(Serialize, Deserialize, Debug)]
    struct TestData {
        period: usize,
        init_len: usize,
        input: Vec<f64>,
        expected: Vec<f64>,
    }

    #[test]
    fn test_ewma() -> Result<(), Error> {
        let file_path = "src/indicators/test_data/indicators/ewma.json";

        match get_test_data::<TestData>(file_path) {
            Ok(test_data) => {
                let mut ewma = EWMA::<f64>::new(test_data.period);
                let mut result: Vec<f64> = Vec::with_capacity(test_data.expected.len());

                for value in test_data.input.iter() {
                    result.push(ewma.next(*value))
                }

                // assert_eq!(result, test_data.expected);
                assert!(assert_vec_nearly_equal(
                    &result,
                    &test_data.expected,
                    0.00000000001
                ));

                Ok(())
            }
            Err(e) => {
                eprintln!("An error occurred: {:?}", e);
                Err(e)
            }
        }
    }
}
