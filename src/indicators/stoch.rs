// TODO: Improve perf
use super::highest::Highest;
use super::lowest::Lowest;
use super::sma::SMA;
use super::traits::IndicatorTraits;
use num_traits::{Float, NumAssign};
use std::fmt::Debug;

#[derive(Debug, Clone)]
pub struct Stoch<T: Float + NumAssign + Debug> {
    period: usize,
    highest: Highest<T>,
    lowest: Lowest<T>,
    smooth_k: SMA<T>,
    smooth_d: SMA<T>,
    t_100: T,
}

impl<T: Float + NumAssign + Debug> Stoch<T> {
    pub fn new(period: usize, smooth_k: usize, smooth_d: usize) -> Self {
        Stoch {
            period,
            highest: Highest::new(period),
            lowest: Lowest::new(period),
            smooth_k: SMA::new(smooth_k),
            smooth_d: SMA::new(smooth_d),
            t_100: T::from(100).unwrap(),
        }
    }

    fn calc(&self, current: T, highest: T, lowest: T) -> T {
        ((current - lowest) / (highest - lowest)) * self.t_100
    }
}

impl<T: Float + NumAssign + Debug> IndicatorTraits<(T, T, T), (T, T)> for Stoch<T> {
    fn get_init_len(&self) -> usize {
        self.period
    }

    #[inline]
    fn next(&mut self, values: (T, T, T)) -> (T, T) {
        let high = values.0;
        let low = values.1;
        let close = values.2;
        let highest = self.highest.next(high);
        let lowest = self.lowest.next(low);
        let stoch_k = if highest.0 != lowest.0 {
            self.calc(close, highest.0, lowest.0)
        } else {
            T::zero()
        };

        let smooth_stoch_k = self.smooth_k.next(stoch_k);
        let smooth_stoch_d = self.smooth_d.next(smooth_stoch_k);
        (smooth_stoch_k, smooth_stoch_d)
    }

    fn live(&self, values: (T, T, T)) -> (T, T) {
        let high = values.0;
        let low = values.1;
        let close = values.2;
        let highest = self.highest.live(high);
        let lowest = self.lowest.live(low);
        let stoch_k = if highest.0 != lowest.0 {
            self.calc(close, highest.0, lowest.0)
        } else {
            T::zero()
        };

        let smooth_stoch_k = self.smooth_k.live(stoch_k);
        let smooth_stoch_d = self.smooth_d.live(smooth_stoch_k);
        (smooth_stoch_k, smooth_stoch_d)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::indicators::test_data::get_test_data;
    use crate::tools::test::assert_tuples2_nearly_equal;
    use serde::{Deserialize, Serialize};
    use std::io::Error;

    #[derive(Serialize, Deserialize, Debug)]
    struct TestData {
        period: usize,
        smooth_k: usize,
        smooth_d: usize,
        init_len: usize,
        input: Vec<(f64, f64, f64)>,
        expected: Vec<(f64, f64)>,
    }

    #[test]
    fn test_stoch() -> Result<(), Error> {
        let file_path = "src/indicators/test_data/indicators/stoch.json";

        match get_test_data::<TestData>(file_path) {
            Ok(test_data) => {
                let mut stoch =
                    Stoch::<f64>::new(test_data.period, test_data.smooth_k, test_data.smooth_d);
                let mut result: Vec<(f64, f64)> = Vec::with_capacity(test_data.expected.len());

                for values in test_data.input.iter() {
                    result.push(stoch.next(*values))
                }

                // assert_eq!(result, test_data.expected);
                assert!(assert_tuples2_nearly_equal(
                    &result,
                    &test_data.expected,
                    0.00000000001
                ));

                Ok(())
            }
            Err(e) => {
                eprintln!("An error occurred: {:?}", e);
                Err(e)
            }
        }
    }
}
