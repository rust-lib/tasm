use super::traits::IndicatorTraits;
use num_traits::{Float, NumAssign};
use std::fmt::Debug;

#[derive(Debug, Clone)]
pub struct RSI<T: Float + NumAssign + Debug> {
    period: usize,
    exponent: T,
    max: T,
    min: T,
    last: T,
    grma: T,
    lrma: T,
}

impl<T: Float + NumAssign + Debug> RSI<T> {
    pub fn new(period: usize) -> Self {
        RSI {
            period,
            exponent: T::one() / T::from(period).unwrap(),
            max: T::from(100).unwrap(),
            min: T::zero(),
            last: T::zero(),
            grma: T::zero(),
            lrma: T::zero(),
        }
    }

    fn calc(&self, value: T) -> (T, T, T) {
        let diff = if self.last > T::zero() {
            value - self.last
        } else {
            T::zero()
        };

        let mut grma = self.grma;
        let mut lrma = self.lrma;

        if diff > T::zero() {
            grma += (diff - grma) * self.exponent;
            lrma += (T::zero() - lrma) * self.exponent;
        } else {
            grma += (T::zero() - grma) * self.exponent;
            lrma += (-diff - lrma) * self.exponent;
        };

        if lrma == T::zero() {
            (self.max, grma, lrma)
        } else if grma == T::zero() {
            (self.min, grma, lrma)
        } else {
            let rsi = self.max - self.max / (T::one() + grma / lrma);
            (rsi, grma, lrma)
        }
    }
}

impl<T: Float + NumAssign + Debug> IndicatorTraits<T, T> for RSI<T> {
    fn get_init_len(&self) -> usize {
        self.period
    }

    #[inline]
    fn next(&mut self, value: T) -> T {
        let (rsi, grma, lrma) = self.calc(value);
        self.last = value;
        self.grma = grma;
        self.lrma = lrma;
        rsi
    }

    fn live(&self, value: T) -> T {
        let (rsi, _, _) = self.calc(value);
        rsi
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::indicators::test_data::get_test_data;
    use crate::tools::test::assert_vec_nearly_equal;
    use serde::{Deserialize, Serialize};
    use std::io::Error;

    #[derive(Serialize, Deserialize, Debug)]
    struct TestData {
        period: usize,
        init_len: usize,
        input: Vec<f64>,
        expected: Vec<f64>,
    }

    #[test]
    fn test_rsi() -> Result<(), Error> {
        let file_path = "src/indicators/test_data/indicators/rsi.json";

        match get_test_data::<TestData>(file_path) {
            Ok(test_data) => {
                let mut rsi = RSI::<f64>::new(test_data.period);
                let mut result: Vec<f64> = Vec::with_capacity(test_data.expected.len());

                for values in test_data.input.iter() {
                    result.push(rsi.next(*values))
                }

                // assert_eq!(result, test_data.expected);
                assert!(assert_vec_nearly_equal(
                    &result,
                    &test_data.expected,
                    0.00000000001
                ));

                Ok(())
            }
            Err(e) => {
                eprintln!("An error occurred: {:?}", e);
                Err(e)
            }
        }
    }
}
