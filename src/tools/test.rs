pub fn nearly_equal(a: f64, b: f64, epsilon: f64) -> bool {
    let abs_a = a.abs();
    let abs_b = b.abs();
    let diff = (a - b).abs();

    if a == b {
        // Handle infinities.
        true
    } else if a == 0.0 || b == 0.0 || (abs_a + abs_b < f64::MIN_POSITIVE) {
        // a or b is zero, or both are extremely close to it. Relative error is less meaningful here.
        diff < epsilon
    } else {
        // Use relative error.
        (diff / f64::min(abs_a + abs_b, f64::MAX)) < epsilon
    }
}

pub fn assert_vec_nearly_equal(v1: &[f64], v2: &[f64], epsilon: f64) -> bool {
    if v1.len() != v2.len() {
        return false;
    }
    for (a, b) in v1.iter().zip(v2) {
        if !nearly_equal(*a, *b, epsilon) {
            return false;
        }
    }
    true
}

pub fn assert_tuples2_nearly_equal(t1: &[(f64, f64)], t2: &[(f64, f64)], epsilon: f64) -> bool {
    if t1.len() != t2.len() {
        return false;
    }
    for ((a, b), (x, y)) in t1.iter().zip(t2) {
        if !(nearly_equal(*a, *x, epsilon) && nearly_equal(*b, *y, epsilon)) {
            return false;
        }
    }
    true
}

pub fn assert_tuples3_nearly_equal(
    t1: &[(f64, f64, f64)],
    t2: &[(f64, f64, f64)],
    epsilon: f64,
) -> bool {
    if t1.len() != t2.len() {
        return false;
    }
    for ((a, b, c), (x, y, z)) in t1.iter().zip(t2) {
        if !(nearly_equal(*a, *x, epsilon)
            && nearly_equal(*b, *y, epsilon)
            && nearly_equal(*c, *z, epsilon))
        {
            return false;
        }
    }
    true
}
