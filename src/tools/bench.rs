use rand::Rng;

#[derive(Debug, PartialEq, Clone)]
pub struct OHLCV {
    pub open: f64,
    pub high: f64,
    pub low: f64,
    pub close: f64,
    pub volume: f64,
    pub volume_buy: f64,
    pub volume_sell: f64,
}

impl OHLCV {
    pub fn generate_random_ohlcv_series(length: usize) -> Vec<OHLCV> {
        let mut rng = rand::thread_rng();
        let mut series = Vec::with_capacity(length);

        let mut prev_close = rng.gen_range(50.0..=100.0);

        for _ in 0..length {
            // Generate random prices between open and 100 for high, and 50 and open for low
            let open = prev_close;
            let close = rng.gen_range(open..=100.0);
            let high = rng.gen_range(open..=100.0);
            let low = rng.gen_range(50.0..=open);

            // Generate random volume between 1000 and 10000
            let volume = rng.gen_range(1000.0..=10000.0);
            let volume_buy = if close >= open {
                rng.gen_range(5000.0..=10000.0)
            } else {
                rng.gen_range(1000.0..=5000.0)
            };
            let volume_sell = if close <= open {
                rng.gen_range(5000.0..=10000.0)
            } else {
                rng.gen_range(1000.0..=5000.0)
            };

            let ohlcv_data = OHLCV {
                open,
                high,
                low,
                close,
                volume,
                volume_buy,
                volume_sell,
            };

            series.push(ohlcv_data);
            prev_close = close;
        }

        series
    }
}
