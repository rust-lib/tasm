use serde::de::DeserializeOwned;
use std::fs::File;
use std::io::Error;
use std::path::Path;

pub fn load_json<T: DeserializeOwned>(file_path: &str) -> Result<T, Error> {
    let path = Path::new(file_path);
    let file = File::open(path)?;
    let data: T = serde_json::from_reader(file)?;
    Ok(data)
}
