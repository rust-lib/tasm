use std::collections::vec_deque::Iter;
use std::collections::VecDeque;
use std::fmt::Debug;

#[derive(Debug, Clone)]
pub struct Cache<T: Debug> {
    size: usize,
    cache: VecDeque<T>,
}

impl<T: Debug> Cache<T> {
    pub fn new(size: usize) -> Self {
        Cache {
            size,
            cache: VecDeque::with_capacity(size),
        }
    }

    pub fn push(&mut self, value: T) -> Option<T> {
        let shifted_value = if self.cache.len() == self.size {
            self.cache.pop_front()
        } else {
            None
        };

        self.cache.push_back(value);
        shifted_value
    }

    pub fn shift(&mut self) -> Option<T> {
        if self.cache.len() == self.size {
            self.cache.pop_front()
        } else {
            None
        }
    }

    pub fn get(&self, index: usize) -> Option<&T> {
        self.cache.get(index)
    }

    pub fn front(&self) -> Option<&T> {
        self.cache.front()
    }

    pub fn back(&self) -> Option<&T> {
        self.cache.back()
    }

    pub fn clear(&mut self) {
        self.cache.clear();
    }

    pub fn len(&self) -> usize {
        self.cache.len()
    }

    pub fn iter(&self) -> Iter<'_, T> {
        self.cache.iter()
    }
}
