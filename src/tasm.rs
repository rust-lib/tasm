pub mod data_point;
pub mod indicators;
pub mod tools;

#[cfg(all(feature = "f32", feature = "f64"))]
compile_error!("Features `f32` and `f64` are mutually exclusive");

#[cfg(not(any(feature = "f32", feature = "f64")))]
compile_error!("Please specify a feature: `f32` or `f64`");

#[cfg(feature = "f32")]
pub type FloatType = f32;

#[cfg(feature = "f64")]
pub type FloatType = f64;
