use rand::distributions::{Distribution, Uniform};
use std::cmp;
use std::time::Instant;
use tasm::indicators::traits::IndicatorTraits;

// use tasm::indicators::sma::SMA;
use tasm::indicators::sma::SMA;

pub struct Timer {
    start_time: Option<Instant>,
}

impl Timer {
    pub fn new() -> Timer {
        Timer { start_time: None }
    }

    pub fn start(&mut self) {
        self.start_time = Some(Instant::now());
    }

    pub fn stop(&mut self, title: String, count: u32) {
        if let Some(start_time) = self.start_time {
            let elapsed = start_time.elapsed();

            let minutes = elapsed.as_secs() / 60;
            let seconds = elapsed.as_secs() % 60;
            let milliseconds = elapsed.subsec_millis();
            let nanoseconds = elapsed.subsec_nanos();
            let per_sec = count / cmp::max(elapsed.as_millis() as u32, 1000) * 1000;

            println!(
                "Benchmark {}: {:01}s {:01}s {:01}ms {:01}ns {}/s",
                title, minutes, seconds, milliseconds, nanoseconds, per_sec
            );
        } else {
            println!("Timer was not started");
        }
    }
}

fn main() {
    let mut timer = Timer::new();
    // let mut sma = SMA::new(20);
    let mut ema = SMA::new(20);
    let sec_in_year = 60 * 60 * 24 * 365;

    let mut rng = rand::thread_rng();
    let between = Uniform::try_from(15000.0..50000.0).unwrap();
    timer.start();
    let data: Vec<f64> = between.sample_iter(&mut rng).take(sec_in_year).collect();
    timer.stop(String::from("Generate 1 year"), sec_in_year as u32);

    timer.start();
    for i in 0..sec_in_year {
        ema.next(data[i]);
    }
    timer.stop(String::from("1 Year"), sec_in_year as u32);
}
