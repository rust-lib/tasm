use polars::prelude::*;
use std::env;

fn read_csv_to_dataframe(csv_file: &str) -> Result<DataFrame, Box<dyn std::error::Error>> {
    let df = CsvReader::from_path(csv_file)?.finish()?;
    Ok(df)
}

fn write_dataframe_to_parquet(
    df: &mut DataFrame,
    parquet_file: &str,
) -> Result<(), Box<dyn std::error::Error>> {
    let writer = ParquetWriter::new(std::fs::File::create(parquet_file).unwrap());

    writer.finish(df)?;

    Ok(())
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Retrieve command-line arguments
    let args: Vec<String> = env::args().collect();

    // Ensure that both CSV file path and Parquet file path are provided
    if args.len() < 3 {
        eprintln!("Usage: cargo run -- <csv_file> <parquet_file>");
        std::process::exit(1);
    }

    // Extract the CSV file path and Parquet file path
    let csv_file = &args[1];
    let parquet_file = &args[2];

    // Read the CSV file into a DataFrame
    let mut df = read_csv_to_dataframe(csv_file)?;

    // Write the DataFrame to a Parquet file
    write_dataframe_to_parquet(&mut df, parquet_file)?;

    Ok(())
}
